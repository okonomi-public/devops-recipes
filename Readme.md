# Okonomi DevOps Recipes

This repository contains recipes to setup infrastructure, deploy applications and organize DevOps assets.

#### 01. Deploy GitLab CI/CD Runner to DigitalOcean Kubernetes

This will allow you to run GitLab CI/CD workloads on a DigitalOcean Kubernetes cluster.

[Open the recipe](recipe-01)
