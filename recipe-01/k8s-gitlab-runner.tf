variable "digitalocean_api_key" {
  type = string
}

variable "digitalocean_spaces_access_id" {
  type = string
}

variable "digitalocean_spaces_secret_key" {
  type = string
}

variable "digitalocean_k8s_region" {
  type = string
  default = "fra1"
}

variable "digitalocean_k8s_version" {
  type = string
  default = "1.16.6-do.2"
}

variable "digitalocean_k8s_node_count" {
  type = number
  default = 3
}

variable "digitalocean_k8s_node_size" {
  type = string
  default = "s-2vcpu-2gb"
}

variable "gitlab_runner_token" {
  type = string
}

variable "gitlab_runner_url" {
  type = string
  default = "https://gitlab.com/"
}

variable "gitlab_runner_namespace" {
  type = string
  default = "gitlab"
}

provider "digitalocean" {
  token = var.digitalocean_api_key

  spaces_access_id  = var.digitalocean_spaces_access_id
  spaces_secret_key = var.digitalocean_spaces_secret_key
}

resource "digitalocean_kubernetes_cluster" "gitlab_runner" {
  name    = "gitlab-ci-runner"
  region  = var.digitalocean_k8s_region
  version = var.digitalocean_k8s_version

  node_pool {
    name       = "worker-pool"
    size       = var.digitalocean_k8s_node_size
    node_count = var.digitalocean_k8s_node_count
  }
}

resource "local_file" "k8s_config" {
  content  = digitalocean_kubernetes_cluster.gitlab_runner.kube_config[0].raw_config
  filename = "kube_config.yaml"
}

resource "digitalocean_spaces_bucket" "gitlab_runner_cache" {
  depends_on = [digitalocean_kubernetes_cluster.gitlab_runner]

  name    = join("-", ["gitlab-runner-cache", digitalocean_kubernetes_cluster.gitlab_runner.id])
  region  = var.digitalocean_k8s_region

  force_destroy = true
}

provider "kubernetes" {
  load_config_file = false
  host  = digitalocean_kubernetes_cluster.gitlab_runner.endpoint
  token = digitalocean_kubernetes_cluster.gitlab_runner.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.gitlab_runner.kube_config[0].cluster_ca_certificate
  )
}

resource "kubernetes_namespace" "gitlab" {
  metadata {
    name = var.gitlab_runner_namespace
  }
}

resource "kubernetes_secret" "gitlab_runner_cache_credentials" {
  depends_on = [kubernetes_namespace.gitlab]

  metadata {
    name = "gitlab-runner-cache-credentials"
    namespace = var.gitlab_runner_namespace
  }

  data = {
    accesskey = var.digitalocean_spaces_access_id
    secretkey = var.digitalocean_spaces_secret_key
  }

  type = "kubernetes.io/generic"
}

provider "helm" {
  kubernetes {
    load_config_file = false
    host  = digitalocean_kubernetes_cluster.gitlab_runner.endpoint
    token = digitalocean_kubernetes_cluster.gitlab_runner.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.gitlab_runner.kube_config[0].cluster_ca_certificate
    )
  }
}

data "helm_repository" "gitlab" {
  name = "gitlab"
  url  = "https://charts.gitlab.io"
}

resource "helm_release" "gitlab_runner_release" {
  depends_on = [digitalocean_kubernetes_cluster.gitlab_runner]

  name            = "gitlab-runner"
  namespace       = var.gitlab_runner_namespace
  repository      = data.helm_repository.gitlab.metadata[0].name
  chart           = "gitlab/gitlab-runner"
  verify          = false

  values = []

  set {
    name  = "runnerRegistrationToken"
    value = var.gitlab_runner_token
  }

  set {
    name  = "gitlabUrl"
    value = var.gitlab_runner_url
  }

  set {
    name  = "rbac.create"
    value = true
  }

  set {
    name  = "runners.cache.cacheType"
    value = "s3"
  }

  set {
    name  = "runners.cache.cachePath"
    value = "gitlab_runner"
  }

  set {
    name  = "runners.cache.cacheShared"
    value = true
  }

  set {
    name  = "runners.cache.s3ServerAddress"
    value = replace(digitalocean_spaces_bucket.gitlab_runner_cache.bucket_domain_name, "/^[^.]+\\./", "")
  }

  set {
    name  = "runners.cache.s3BucketName"
    value = digitalocean_spaces_bucket.gitlab_runner_cache.name
  }

  set {
    name  = "runners.cache.s3BucketLocation"
    value = digitalocean_spaces_bucket.gitlab_runner_cache.region
  }

  set {
    name  = "runners.cache.s3CacheInsecure"
    value = false
  }

  set {
    name  = "runners.cache.secretName"
    value = kubernetes_secret.gitlab_runner_cache_credentials.metadata[0].name
  }
}
