# Recipe 01 - Run GitLab CI on DigitalOcean Kubernetes

The goal of this recipe is to deploy a [GitLab Runner Kubernetes Executor](https://docs.gitlab.com/runner/executors/kubernetes.html) into a DigitalOcean Kubernetes cluster.

Using Terraform, we will do the following:

1. Create a new [DigitalOcean Kubernetes](https://www.digitalocean.com/products/kubernetes/) cluster
2. Create a new [DigitalOcean Space](https://www.digitalocean.com/products/spaces/) which will act as a s3 [Cache](https://docs.gitlab.com/ee/ci/caching/) between GitLab CI/CD jobs
3. Deploy the [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html) through the [Terraform Helm Provider](https://www.terraform.io/docs/providers/helm/index.html)

## Prerequisites

First, checkout the repository to your computer.

```
git clone https://gitlab.com/okonomi-public/devops-recipes.git
```

From the root of this repository, navigate to this recipe:

```
cd recipe-01
```

In order to follow the upcoming steps, you need a [DigitalOcean](https://digitalocean.com) and a [GitLab](https://about.gitlab.com) account. From these accounts, you need to collect some keys and tokens. If you don't have accounts yet, follow the links to sign up.

In order to supply these information to our Terraform configuration, make a copy of the `k8s-GitLab-runner.tfvars.sample` file.

```
cp k8s-gitlab-runner.tfvars.sample k8s-gitlab-runner.tfvars
```

### DigitalOcean

From your DigitalOcean account go to "Manage / API" and generate a new API token.

![Generate DigitalOcean API Token](media/do_generate_token.png)

Add the newly generated token to your vars file (k8s-gitlab-runner.tfvars)

```
digitalocean_api_key = <TOKEN>
```

Now scroll down to the section "Spaces access keys" and generate a new key.

![Generate DigitalOcean API Token](media/do_generate_spaces_credentials.png)

Add the newly generated access id and secret key to your vars file (k8s-GitLab-runner.tfvars):

```
digitalocean_spaces_access_id = <ACCESS_ID>
digitalocean_spaces_secret_key = <SECRET_KEY>
```

### GitLab

We need a project with a `.gitlab-ci.yml` to test our setup later. For your convenience you can simply fork this [example project](https://gitlab.com/okonomi-public/simple-ci-example).

Within your GitLab project, go to "Settings / CI/CD" and expand the Runners sections. First, disable shared runners. Then copy the registration token from the left hand side.

![Retriebe GitLab Runner Token](media/gitlab_ci_runner.png)

Add the runner registration token to your vars file (k8s-gitlab-runner.tfvars):

```
gitlab_runner_token = <RUNNER_TOKEN>
```

## Run the recipe

After you collected all necessary keys and tokens, it is time to startup the recipes Docker container. From the root folder run:

```
./cli.sh
```

This started a shell within the Docker container and you can now navigate to this recipes' folder:

```
cd recipe-01
```

Before we can apply the configuration file, we need to init Terraform to download the required providers.

```
terraform init
```

If you followed the prerequisites, you can now run Terraform to apply the configuration file. Enter 'yes' when prompted.

```
terraform apply -var-file=k8s-gitlab-runner.tfvars
```

This will now run for about 5 minutes, spinning up the Kubernetes Cluster and deploying the GitLab runner. It will conclude with the message:

```
...
Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```

When Terraform completed, you can check if the Helm Chart has been properly installed. Run:

```
helm ls -n gitlab
```

Which should output something like this:

```
NAME         	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART               	APP VERSION
gitlab-runner	gitlab   	1       	2020-04-01 16:40:19.106693924 +0000 UTC	deployed	gitlab-runner-0.15.0	12.9.0     
```

When you go back to the Runners sections in "Settings / CI/CD" of your project in GitLab, you should now see the activated runner for the project.

![GitLab CI/CD Runners](media/gitlab_ci_runner_installed.png)

The cluster has now been setup and the GitLab Runner has been deployed and configured. We can now run the first pipeline on our setup.

![Run Pipeline](media/gitlab_run_build.gif)

If you used the provided sample project, you can see from the job logs that it uploaded files to the cache. On subsequent jobs it will also pull files from the cache at the beginning of the execution.

## Cleanup

If you don't need the setup anymore, you can cleanup everything with this command:

```
terraform destroy -var-file=k8s-gitlab-runner.tfvars
```
